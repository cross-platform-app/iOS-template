cmake_minimum_required(VERSION 3.0)

# set target platform. used in preprocessor of gl-example to choose correct include headers
set(TARGET_PLATFORM IPHONE)

add_subdirectory(Client)
add_subdirectory(gl-example)
add_subdirectory(skia-example)
add_subdirectory(graphics-interface)
add_subdirectory(Core)

