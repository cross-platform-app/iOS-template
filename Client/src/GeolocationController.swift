
import UIKit

/// `OnChangeListener` that calls back into the UI-thread when new location-data is available.
class OnLocationChangedListener: CROnChangeListener {
    private var controller: GeolocationController!
    
    /// - parameters:
    ///     - controller: the controller-instance that the callbacks should be executed on.
    init(controller: GeolocationController) {
        self.controller = controller
    }
    
    func onSuccess() {
        DispatchQueue.main.async {
            self.controller.setStateUpdateLocationLoading(loading: false)
            self.controller.setUpdateLocationErrorVisible(visible: false)
            self.controller.updateLocationDisplay()
        }
    }
    
    func onError() {
        DispatchQueue.main.async {
            self.controller.setStateUpdateLocationLoading(loading: false)
            self.controller.setUpdateLocationErrorVisible(visible: true)
        }
    }
    
    
}

/// Controller that demonstrates how to consume a system-API like the geolocation through an abstraction-layer
/// provided by the Logic-Tier.
class GeolocationController: UIViewController {
    @IBOutlet var buttonUpdateLocation: UIButton!
    @IBOutlet var labelUpdateLocationError: UILabel!
    @IBOutlet var activityIndicatorUpdateLocation: UIActivityIndicatorView!
    @IBOutlet var labelLatitude: UILabel!
    @IBOutlet var labelLongitude: UILabel!
    
    private var locationService: CRLocationServiceInterface!
    
    override func viewDidLoad() {
        buttonUpdateLocation.layer.cornerRadius = 20
        locationService = CRLocationServiceInterface.locationService(IOSLocation.init())
        locationService.onLocationChanged(OnLocationChangedListener.init(controller: self))
        setStateUpdateLocationLoading(loading: false)
    }
    
    /// This function gets called by the _Update Location_-button.
    @IBAction func onButtonUpdateLocationClicked(_ sender: UIButton) {
        setStateUpdateLocationLoading(loading: true)
        DispatchQueue.global(qos: .userInitiated).async {
            self.locationService.updateLocation()
        }
    }
    
    /// Updates the labels that display the current location.
    /// Requests the new Location on the `locationService` and renders the values as strings.
    func updateLocationDisplay() {
        let location = locationService.getLocation()
        labelLatitude.text = String(location.lat)
        labelLongitude.text = String(location.lon)
    }
    
    
    /// Sets the state of the UI elements related to fetching the device-location.
    /// - parameters:
    ///     - loading:
    ///         The UI can have two states:
    ///         - `true`:   The new location is currently being loaded. This means that the UI waits for
    ///                     a system callback and the user can not interact with the _Update Location_-Button.
    ///                     Additionally a rotating progress-indicator is giving feedback to the user.
    ///         - `false`:  The user can request a new location-update. This means that the button can be clicked.
    func setStateUpdateLocationLoading(loading: Bool) {
        if(loading) {
            activityIndicatorUpdateLocation.isHidden = false
            activityIndicatorUpdateLocation.startAnimating()
            buttonUpdateLocation.isEnabled = false
        } else {
            activityIndicatorUpdateLocation.isHidden = true
            activityIndicatorUpdateLocation.stopAnimating()
            buttonUpdateLocation.isEnabled = true
        }
    }
    
    /// Hides/shows the error-message that indicates that something went wrong while requesting
    /// the location
    /// - parameters:
    ///     - visible:
    ///         - `true`: the error-message is visible
    ///         - `false`: the error-message is hidden
    func setUpdateLocationErrorVisible(visible: Bool) {
        labelUpdateLocationError.isHidden = !visible
    }
}
