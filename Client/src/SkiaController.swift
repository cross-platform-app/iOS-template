
import UIKit
import GLKit

/// Controller for the _Skia_ rendering example.
class SkiaController: UIViewController, GLKViewDelegate {
    
    @IBOutlet var skiaExampleView: GLKView!
    
    private var skiaExample: SKSkiaExampleInterface!
    private var displayLink: CADisplayLink!
    
    private var needsRezize: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        skiaExampleView.context = EAGLContext.init(api: EAGLRenderingAPI.openGLES2)
        skiaExampleView.delegate = self
        
        // setting up the rendering loop
        displayLink = CADisplayLink.init(target: skiaExampleView, selector: #selector(CALayer.display))
        displayLink.preferredFramesPerSecond = 60
    }
    
    func glkView(_ view: GLKView, drawIn rect: CGRect) {
        // glExample needs to be initialized inside here, because its initialization process
        // includes some OpenGL calls.
        // Inside glkView we can be sure that the Context has been created and that the correct
        // current context is set.
        if(skiaExample == nil) {
            skiaExample = SKSkiaExampleInterface.skiaExample()
        }
        // the viewport needs to be set in each frame, because a rotation or a resize of the rendering area
        // could have been occurred since the last frame.
        if(needsRezize) {
            skiaExample.setViewport(Int32(view.drawableWidth), height: Int32(view.drawableHeight));
            needsRezize = false;
        }
        // calling the actual rendering-code from `gl-example`
        skiaExample.render()
        
    }
    
    override func viewWillLayoutSubviews() {
        needsRezize = true;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        displayLink.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        displayLink.remove(from: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    
}

