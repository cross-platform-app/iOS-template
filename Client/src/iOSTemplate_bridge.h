#pragma once

#include "core/CRCounterInterface.h"
#include "core/CROnChangeListener.h"
#include "glExample/GEGlExampleInterface.h"
#include "skiaExample/SKSkiaExampleInterface.h"
#include "core/CRLocationRecord.h"
#include "core/CRLocationServiceInterface.h"
#include "core/CRLocationInterface.h"
#include "graphics/CRGraphicsInterface.h"
#include "graphics/CRCanvasInterface.h"
#include "graphics/CRStyle.h"
#include "graphics/CRPaint.h"
#include "graphics/CRColor.h"