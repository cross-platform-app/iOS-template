
import UIKit
import GLKit

/// Controller for the _OpenGL ES_ rendering example.
class OpenGLController: UIViewController, GLKViewDelegate {
    
    @IBOutlet var glExampleView: GLKView!
    private var glExample: GEGlExampleInterface!
    private var displayLink: CADisplayLink!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        glExampleView.context = EAGLContext.init(api: EAGLRenderingAPI.openGLES2)
        glExampleView.delegate = self
        
        // setting up the rendering loop
        displayLink = CADisplayLink.init(target: glExampleView, selector: #selector(CALayer.display))
        displayLink.preferredFramesPerSecond = 60
        
    }
    
    func glkView(_ view: GLKView, drawIn rect: CGRect) {
        // glExample needs to be initialized inside here, because its initialization process
        // includes some OpenGL calls.
        // Inside glkView we can be sure that the Context has been created and that the correct
        // current context is set.
        if(glExample == nil) {
            glExample = GEGlExampleInterface.glExample()
            // set background color to white to blend in with the application background.
            glExample.setColor(1, green: 1, blue: 1)
        }
        // the viewport needs to be set in each frame, because a rotation or a resize of the rendering area
        // could have been occurred since the last frame.
        glExample.setViewport(Int32(view.drawableWidth), height: Int32(view.drawableHeight));
        // calling the actual rendering-code from `gl-example`
        glExample.render()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        displayLink.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        displayLink.remove(from: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    
}
