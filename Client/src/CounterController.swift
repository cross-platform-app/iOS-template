
import UIKit

/// `OnChangeListener` for the `CounterController`.
/// calls back to the UI-Thread when a state-change has occurred on the counter.
class OnCounterChangedListener: CROnChangeListener {
    private var controller: CounterController!
    
    /// - parameters:
    ///     - counterController:    the controller-instance on which the UI-updates
    ///                             should be performed.
    init(controller: CounterController) {
        self.controller = controller
    }
    
    func onSuccess() {
        DispatchQueue.main.async {
            self.controller.setErrorMessageVisible(visible: false)
            self.controller.setStateCounterLoading(loading: false)
            self.controller.updateCounterDisplay()
            
        }
    }
    
    func onError() {
        DispatchQueue.main.async {
            self.controller.setErrorMessageVisible(visible: true)
            self.controller.setStateCounterLoading(loading: false)
        }
    }
}

/// Controller for the Counter-example.
/// This example shows how to interact with a C++-based library in an asynchronous matter.
class CounterController: UIViewController {
    @IBOutlet var labelCounter: UILabel!
    @IBOutlet var buttonIncreaseCounter: UIButton!
    @IBOutlet var buttonResetCounter: UIButton!
    @IBOutlet var labelCounterError: UILabel!
    @IBOutlet var activityIndicatorCounterLoading: UIActivityIndicatorView!
    
    private var counter: CRCounterInterface!
    
    override func viewDidLoad() {
        buttonIncreaseCounter.layer.cornerRadius = 20
        buttonResetCounter.layer.cornerRadius = 20
        
        /// Initializing the counter and registering the `OnChangeListener` to get informed on state-changes
        counter = CRCounterInterface.counter()
        counter.onNumberChanged(OnCounterChangedListener.init(controller: self))
        
        setStateCounterLoading(loading: false)
    }
    
    /// Gets called by the _Increase Counter_-Button in the UI when the user clicks the button.
    @IBAction func buttonIncreaseCounterClicked(_ sender: UIButton) {
        setStateCounterLoading(loading: true)
        DispatchQueue.global(qos: .userInitiated).async {
            self.counter.increaseNumber()
        }
    }
    
    /// Gets called by the _Reset_-Button in the UI when the user clicks the button.
    @IBAction func buttonResetCounterClicked(_ sender: UIButton) {
        setStateCounterLoading(loading: true)
        DispatchQueue.global(qos: .userInitiated).async {
            self.counter.resetNumber()
        }
    }
    
    
    /// Sets the state of the UI-elements in the example. The elements can have two different states.
    /// - parameters:
    ///     - loading:
    ///         - `true`:   the user is not allowed to interact with the elements, because the last
    ///                     asynchronous state-change has not yet called back to the UI. This means
    ///                     that the buttons can not be clicked and a rotating progress-indicator
    ///                     is showing up.
    ///         - `false`:  the user can interact with the elements. This means that he can click
    ///                     the _Increase Counter_-Button and the _Reset_-Button.
    func setStateCounterLoading(loading: Bool) {
        if(loading) {
            activityIndicatorCounterLoading.isHidden = false
            activityIndicatorCounterLoading.startAnimating()
            buttonResetCounter.isEnabled = false
            buttonIncreaseCounter.isEnabled = false
        } else {
            activityIndicatorCounterLoading.isHidden = true
            activityIndicatorCounterLoading.stopAnimating()
            buttonResetCounter.isEnabled = true
            buttonIncreaseCounter.isEnabled = true
        }
    }
    
    /// Shows and hides the error message that tells something went
    /// wrong when increasing the counter.
    /// - parameters:
    ///     - visible: if `true`, the error message is made visible, if `false`, the error message is hidden
    func setErrorMessageVisible(visible: Bool) {
        labelCounterError.isHidden = !visible
    }
    
    /// Updates the label in the user-interface that tells the current
    /// state of the counter. Call this when `onSuccess()` has been called in an `CROnChangeListener`
    /// registered in `counter`.
    func updateCounterDisplay() {
        labelCounter.text = String(counter.getNumber())
    }
    
}
